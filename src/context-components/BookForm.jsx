import React from "react";
import { BookContext } from "../contexts/BookContext";
import { uuid } from "uuid/v1";

export const BookForm = () => {
  const { addBook } = React.useContext(BookContext);
  const [title, setTitle] = React.useState("");
  const [author, setAuthor] = React.useState("");
  const handleTitleChange = evt => {
    setTitle(evt.target.value);
  };
  const handleAuthorChange = evt => {
    setAuthor(evt.target.value);
  };
  const handleSubmit = evt => {
    evt.preventDefault();
    addBook({ title, author, id: uuid() });
    setTitle("");
    setAuthor("");
  };
  return (
    <form onSubmit={handleSubmit}>
      <label>Title</label>
      <input type="text" value={title} onChange={handleTitleChange} />
      <label>Author</label>
      <input type="text" value={author} onChange={handleAuthorChange} />
      <input type="submit" value="Add form" />
    </form>
  );
};
