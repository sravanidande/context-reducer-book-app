import React from "react";
import { BookContext } from "../contexts/BookContext";

const Navbar = () => {
  const { books } = React.useContext(BookContext);
  return (
    <div className="navbar">
      <h1>Ninja Reading</h1>
      <p>current you have {books.length} books to go through... </p>
    </div>
  );
};

export default Navbar;
