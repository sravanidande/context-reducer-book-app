import React from "react";
import { BookContext } from "../contexts/BookContext";
import { BookDetail } from "./BookDetail";

export const BookList = () => {
  const { books } = React.useContext(BookContext);
  return books.length ? (
    <div className="book-list">
      <ul>
        {books.map(book => (
          <BookDetail book={book} key={book.id} />
        ))}
      </ul>
    </div>
  ) : (
    <div className="empty">No books to read. Hello free time.</div>
  );
};

export default BookList;
