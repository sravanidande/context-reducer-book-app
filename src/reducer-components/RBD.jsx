import React from "react";
import { reducerBookContext } from "./RBContext";

export const ReducerBookDetail = ({ book }) => {
  const { dispatch } = React.useContext(reducerBookContext);
  return (
    <li onClick={() => dispatch({ type: "removeBook", id: book.id })}>
      <div className="title">{book.title}</div>
      <div className="author">{book.author}</div>
    </li>
  );
};
