import React from "react";

import { reducerBookContext } from "./RBContext";

export const ReducerNavbar = () => {
  const { books } = React.useContext(reducerBookContext);
  return (
    <div className="navbar">
      <h1>Ninja Reading</h1>
      <p>current you have {books.length} books to go through... </p>
    </div>
  );
};
