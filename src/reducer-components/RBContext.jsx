import React from "react";

export const reducerBookContext = React.createContext();

const bookReducer = (state, action) => {
  // eslint-disable-next-line default-case
  switch (action.type) {
    case "addBook":
      return [...state, action.book];
    case "removeBook":
      return state.filter(book => book.id !== action.id);
    default:
      return state;
  }
};

export const ReducerBookProvider = props => {
  const [books, dispatch] = React.useReducer(bookReducer, []);
  return (
    <reducerBookContext.Provider value={{ books, dispatch }}>
      {props.children}
    </reducerBookContext.Provider>
  );
};
