import React from "react";
import { reducerBookContext } from "./RBContext";

export const ReducerBookForm = () => {
  const { dispatch } = React.useContext(reducerBookContext);
  const [title, setTitle] = React.useState("");
  const [author, setAuthor] = React.useState("");
  const handleTitleChange = evt => {
    setTitle(evt.target.value);
  };
  const handleAuthorChange = evt => {
    setAuthor(evt.target.value);
  };
  const handleSubmit = evt => {
    evt.preventDefault();
    dispatch({ type: "addBook", book: { title, author } });
    setTitle("");
    setAuthor("");
  };
  return (
    <form onSubmit={handleSubmit}>
      <label>Title</label>
      <input type="text" value={title} onChange={handleTitleChange} />
      <label>Author</label>
      <input type="text" value={author} onChange={handleAuthorChange} />
      <input type="submit" value="Add form" />
    </form>
  );
};
