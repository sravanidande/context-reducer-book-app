import React from "react";
import { ReducerBookDetail } from "./RBD";

import { reducerBookContext } from "./RBContext";

export const ReducerBookList = () => {
  const { books } = React.useContext(reducerBookContext);
  return books.length ? (
    <div className="book-list">
      <ul>
        {books.map(book => (
          <ReducerBookDetail book={book} key={book.id} />
        ))}
      </ul>
    </div>
  ) : (
    <div className="empty">No books to read. Hello free time.</div>
  );
};
