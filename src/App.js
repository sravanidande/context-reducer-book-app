import React from "react";

// import { BookContextProvider } from "./contexts/BookContext";
// import Navbar from "./context-components/Navbar";
// import BookList from "./context-components/BookList";
// import { BookForm } from "./context-components/BookForm";
import { ReducerBookProvider } from "./reducer-components/RBContext";
import { ReducerNavbar } from "./reducer-components/RNavbar";
import { ReducerBookForm } from "./reducer-components/RBF";
import { ReducerBookList } from "./reducer-components/RBL";

function App() {
  // return (
  //   <BookContextProvider>
  //     <Navbar />
  //     <BookList />
  //     <BookForm />
  //   </BookContextProvider>
  // );
  return (
    <ReducerBookProvider>
      <ReducerNavbar />
      <ReducerBookList />
      <ReducerBookForm />
    </ReducerBookProvider>
  );
}

export default App;
