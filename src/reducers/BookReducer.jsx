import uuid from "uuid/v1";

export const BookReducer = (state, action) => {
  // eslint-disable-next-line default-case
  switch (action.type) {
    case "addBook":
      return [...state, action.book];

    case "removeBook":
      return state.filter(book => book.id !== action.id);
    default:
      return state;
  }
};
